package dev.team1.serviceTest;

import dev.team1.daos.AdminRepository;
import dev.team1.daos.RestaurantRepository;
import dev.team1.models.Restaurant;
import dev.team1.services.AdminService;
import dev.team1.services.RestaurantService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
public class RestaurantServiceTest {

    private MockMvc mockMvc;

    @MockBean
    RestaurantRepository restaurantRepository;

    @Autowired
    private RestaurantService restaurantService;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(restaurantRepository).build();
    }

    @Test
    public void createRestaurantNotNullTest() {
        Restaurant restaurant = new Restaurant();
        restaurant.setId(1);
        restaurant.setName("Marc");
        restaurant.setAddress("123 Fake Street");
        restaurant.setPhoto("MyPhoto");
        restaurant.setRating("5");
        restaurant.setPlaceId("PlaceId");
        doReturn(restaurant).when(restaurantRepository).save(restaurant);
        assertNotNull(restaurantService.createNewRestaurant(restaurant));
    }

    @Test
    public void getAllRestaurantsTest() {
        //List<Restaurant> restaurantList = Arrays.asList(
                //new Restaurant(1, "Marc", "123 Fake Street", "5","MarcPhoto","MarcId"),
                //new Restaurant(2, "Trent", "456 Fake Street", "4","TrentPhoto","TrentId"),
                //new Restaurant(3, "Melanie", "789 Fake Street", "3","MelaniePhoto","MelanieId")
        //);

        //doReturn(restaurantList).when(restaurantRepository).findAll();
//        assertEquals("Marc", restaurantService.getAllRestaurants().get(0).getName());
//        assertEquals("Trent", restaurantService.getAllRestaurants().get(1).getName());
//        assertEquals("Melanie", restaurantService.getAllRestaurants().get(2).getName());
    }

    @Test
    public void deleteRestaurantTest() {
        Restaurant restaurant = new Restaurant();
        restaurant.setId(1);
        restaurant.setName("Marc");
        restaurant.setAddress("123 Fake Street");
        restaurant.setPhoto("MyPhoto");
        restaurant.setRating("5");
        restaurant.setPlaceId("PlaceId");
        doNothing().when(restaurantRepository).delete(restaurant);
        assertNotNull(restaurantService.deleteRestaurant(restaurant));

    }
}