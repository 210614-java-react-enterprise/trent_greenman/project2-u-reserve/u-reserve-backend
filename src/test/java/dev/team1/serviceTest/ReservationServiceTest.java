package dev.team1.serviceTest;

import dev.team1.daos.ReservationRepository;
import dev.team1.models.Reservation;
import dev.team1.models.Restaurant;
import dev.team1.models.TableReserve;
import dev.team1.services.ReservationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@SpringBootTest
public class ReservationServiceTest {

    private MockMvc mockMvc;

    @MockBean
    ReservationRepository reservationRepository;

    @Autowired
    private ReservationService reservationService;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(reservationRepository).build();
    }

    @Test
    public void createReservationNotNullTest() {
        Restaurant restaurant = new Restaurant();
        TableReserve table = new TableReserve();
        Reservation reservation = new Reservation();
        reservation.setId(1);
        reservation.setRestaurant(restaurant);
        reservation.setEmail("marc@test.test");
        reservation.setStatus("ACCEPTED");
        reservation.setTable(table);
        reservation.setReservationDatetime(LocalDateTime.now());
        doReturn(reservation).when(reservationRepository).save(reservation);
        assertNotNull(reservationService.createNewReservation(reservation));
    }

    @Test
    public void getAllReservationsTest() {
        Restaurant restaurant = new Restaurant();
        TableReserve table = new TableReserve();
        List<Reservation> resList = Arrays.asList(
                new Reservation(1,restaurant,"ACCEPTED", "marc@test.test", LocalDateTime.now(),table),
                new Reservation(2,restaurant,"DECLINED", "trent@test.test", LocalDateTime.now(), table),
                new Reservation(3,restaurant,"PENDING", "melanie@test.test", LocalDateTime.now(),table)
        );

        doReturn(resList).when(reservationRepository).findAll();
        assertEquals("ACCEPTED", reservationService.getAllReservations().get(0).getStatus());
        assertEquals("DECLINED", reservationService.getAllReservations().get(1).getStatus());
        assertEquals("PENDING", reservationService.getAllReservations().get(2).getStatus());
    }

    @Test
    public void getReservationByStatusTest() {
        Reservation reservationKey = new Reservation();
        reservationKey.setId(1);
        reservationKey.setEmail("marc@test.test");
        reservationKey.setStatus("ACCEPTED");
        List<Reservation> resList = new ArrayList<>();
        resList.add(reservationKey);
        doReturn(resList).when(reservationRepository).findByStatus("ACCEPTED");
        assertEquals("marc@test.test",reservationService.getReservationsByStatus("ACCEPTED").get(0).getEmail());
    }

    @Test
    public void getReservationByIdTest() {
        Reservation reservationKey = new Reservation();
        reservationKey.setId(1);
        reservationKey.setEmail("marc@test.test");
        reservationKey.setStatus("ACCEPTED");
        doReturn(reservationKey).when(reservationRepository).findById(1);
        assertEquals("marc@test.test", reservationService.getReservationById(1).getEmail());
    }


    @Test
    public void updateReservationTest() {
//        Reservation reservation = new Reservation();
//        reservation.setId(1);
//        reservation.setEmail("marc@test.test");
//        reservation.setStatus("ACCEPTED");
//        doReturn(reservation).when(reservationRepository).save(reservation);
//        reservation.setStatus("PENDING");
//        doNothing().when(reservationRepository).
//        assertEquals("PENDING",reservationService.getReservationById(1).getStatus());
    }

    @Test
    public void deleteReservation() {
//        Reservation reservation = new Reservation();
//        reservation.setId(1);
//        reservation.setEmail("marc@test.test");
//        reservation.setStatus("ACCEPTED");
//        when(reservationService.deleteReservation(reservation));
//        verify(reservationRepository,times(1)).findAll();
    }
}

