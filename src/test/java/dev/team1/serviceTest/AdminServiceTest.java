package dev.team1.serviceTest;

import dev.team1.daos.AdminRepository;
import dev.team1.models.Administrator;
import dev.team1.services.AdminService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
public class AdminServiceTest {

    private MockMvc mockMvc;

    @MockBean
    AdminRepository adminRepository;

    @Autowired
    private AdminService adminService;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(adminRepository).build();
    }

    @Test
    public void createAdminNotNullTest() {
        Administrator admin = new Administrator();
        admin.setId(1);
        admin.setUsername("Marc");
        admin.setPassword("Test");
        doReturn(admin).when(adminRepository).save(admin);
        assertNotNull(adminService.createNewAdmin(admin));
    }

    @Test
    public void getAdminTest() {
        Administrator admin = new Administrator();
        admin.setId(1);
        admin.setUsername("Marc");
        admin.setPassword("Test");
        doReturn(admin).when(adminRepository).findByUsernameAndPassword("Marc", "Test");
        assertEquals("Marc",adminService.getAdminByUsernameAndPassword("Marc", "Test").getUsername());

    }

    @Test
    public void getAllAdminTest() {
        List<Administrator> adminList = Arrays.asList(
                new Administrator(1,"Marc","Test"),
                new Administrator(2, "Trent", "Test"),
                new Administrator(3, "Melanie", "Test"));

        doReturn(adminList).when(adminRepository).findAll();
        assertEquals("Marc", adminService.getAllAdmin().get(0).getUsername());
        assertEquals("Trent", adminService.getAllAdmin().get(1).getUsername());
        assertEquals("Melanie", adminService.getAllAdmin().get(2).getUsername());
    }

    @Test
    public void deleteAdminTest() {
   Administrator admin = new Administrator(1,"Marc","Test");
        doNothing().when(adminRepository).delete(admin);
        assertNotNull(adminService.deleteAdmin(admin));

    }
}
