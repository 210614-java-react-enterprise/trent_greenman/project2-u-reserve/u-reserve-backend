package dev.team1.serviceTest;

import dev.team1.daos.TableRepository;
import dev.team1.models.Restaurant;
import dev.team1.models.TableReserve;
import dev.team1.services.TableService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.doReturn;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
public class TableServiceTest {

    private MockMvc mockMvc;
    Restaurant restaurant = new Restaurant(25, "La Hustle Inn", "Hustle Ave", "", null, null, 0, 0);

    @MockBean
    TableRepository tableRepository;

    @Autowired
    private TableService tableService;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(tableRepository).build();
    }

    @Test
    public void createTableNotNullTest() {
        TableReserve table = new TableReserve();
        table.setId(1);
        table.setCapacity(10);
        //table.setRestaurantId(1);

        doReturn(table).when(tableRepository).save(table);
        assertNotNull(tableService.createNewTable(table));
    }


    @Test
    public void getAllTablesTest() {

        List<TableReserve> tableList = Arrays.asList(
                new TableReserve(1, restaurant, 6, "PENDING"),
                new TableReserve(2, restaurant, 7, "APPROVED"),
                new TableReserve(3, restaurant, 8, "DENIED")
        );

        doReturn(tableList).when(tableRepository).findByRestaurant(restaurant);
        assertEquals(6, tableService.getAllTables(restaurant).get(0).getCapacity());
        assertEquals(7, tableService.getAllTables(restaurant).get(1).getCapacity());
        assertEquals(8, tableService.getAllTables(restaurant).get(2).getCapacity());
    }
}