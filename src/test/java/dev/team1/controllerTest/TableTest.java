package dev.team1.controllerTest;

import dev.team1.controllers.AdminController;
import dev.team1.controllers.ReservationController;
import dev.team1.controllers.TableController;
import dev.team1.models.Restaurant;
import dev.team1.models.TableReserve;
import dev.team1.services.AdminService;
import dev.team1.services.ReservationService;
import dev.team1.services.RestaurantService;
import dev.team1.services.TableService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
public class TableTest {

    private MockMvc mockMvc;
    private final Restaurant restaurant = new Restaurant(25, "La Hustle Inn", "Hustle Ave", "", null, null, 0, 0);

    @MockBean
    TableService tableService;

    @Autowired
    TableController tableController;

    @BeforeEach
    public void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(tableController).build();
    }

    @Test
    public void returnAllTablesTest() throws Exception {
        List<TableReserve> tableList = Arrays.asList(
                new TableReserve(1, restaurant, 3, "APPROVED"),
                new TableReserve(1, restaurant, 3, "DENIED"),
                new TableReserve(1, restaurant, 3, "PENDING"));


        doReturn(tableList).when(tableService).getAllTables(restaurant);

        this.mockMvc.perform(get("/table"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk());
    }

    @Test
    public void returnTablesByRestaurantId() throws Exception {
//        List<TableReserve> tableList = Arrays.asList(
//                new TableReserve(1, 3, 3),
//                new TableReserve(2, 3, 4),
//                new TableReserve(3, 3,5));
//
//
//        doReturn(tableList).when(tableService).getTableByRestaurantId(3);
//
//        this.mockMvc.perform(get("/table?restaurantId=3"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$[*].capacity").value(hasItems(3,4,5)));
    }

    @Test
    public void createTableTest() throws Exception {

    }

    @Test
    void deleteTableTest() throws Exception {

    }


}
