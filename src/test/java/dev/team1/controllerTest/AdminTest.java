package dev.team1.controllerTest;

import dev.team1.controllers.AdminController;
import dev.team1.models.Administrator;
import dev.team1.services.AdminService;
import dev.team1.services.ReservationService;
import dev.team1.services.RestaurantService;
import dev.team1.services.TableService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
public class AdminTest {

    private MockMvc mockMvc;

    @MockBean
    AdminService adminService;

    @Autowired
    AdminController adminController;

    @BeforeEach
    public void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(adminController).build();
    }

    @Test
    void adminBadLoginTest() throws Exception {
        Administrator admin = new Administrator(0, "asdfasdf", "1234");
        doReturn(admin).when(adminService).getAdminByUsernameAndPassword(admin.getUsername(), admin.getPassword());

        this.mockMvc.perform(post("/admin")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .header("Authorization", "token")
                .content("{ \"username\":\"Mac\", \"password\":\"1234\" }"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().is(401));
    }

    @Test
    void adminGoodLoginTest() throws Exception {
        Administrator admin = new Administrator(4, "mac", "1234");
        doReturn(admin).when(adminService).getAdminByUsernameAndPassword(admin.getUsername(), admin.getPassword());

        this.mockMvc.perform(post("/admin")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .header("Authorization", "token")
                .content("{ \"id\":4, \"username\":\"mac\", \"password\":\"1234\" }"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().is(200));
    }

}
