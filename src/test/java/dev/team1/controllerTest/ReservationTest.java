package dev.team1.controllerTest;

import dev.team1.controllers.ReservationController;
import dev.team1.models.Reservation;
import dev.team1.models.Restaurant;
import dev.team1.models.TableReserve;
import dev.team1.services.AdminService;
import dev.team1.services.ReservationService;
import dev.team1.services.RestaurantService;
import dev.team1.services.TableService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
public class ReservationTest {

    private MockMvc mockMvc;
    Restaurant restaurant = new Restaurant(25, "La Hustle Inn", "Hustle Ave", "", null, null, 0, 0);
    TableReserve table = new TableReserve(25, restaurant, 10, null);

    @MockBean
    ReservationService resService;

    @Autowired
    ReservationController resController;

    @BeforeEach
    public void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(resController).build();
    }

    @Test
    public void returnUnauthorizedReservationTest() throws Exception {
        TableReserve table = new TableReserve();
        List<Reservation> reservationKeys = Arrays.asList(
                new Reservation(1, restaurant, "Accepted", "Email", LocalDateTime.now(), table),
                new Reservation(2, restaurant, "Declined", "Email", LocalDateTime.now(), table),
                new Reservation(3, restaurant, "Pending", "Email", LocalDateTime.now(), table));


        doReturn(reservationKeys).when(resService).getAllReservations();

        this.mockMvc.perform(get("/reservation?adminid=0")
                .header("Authorization", "token"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isUnauthorized());
    }

//    @Test
//    public void returnAllPendingReservationTest() throws Exception {
//        //int id, Restaurant restaurant, String status, String email, LocalDateTime reservationDatetime, TableReserve table
//        List<Reservation> reservationKey = Arrays.asList(new Reservation(1, restaurant, "PENDING", "Email", LocalDateTime.now(), table));
//
//
//        doReturn(reservationKey).when(resService).getReservationsByStatus("PENDING");
//
//        this.mockMvc.perform(get("/reservation?status=PENDING"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$[*].status").value(hasItems("PENDING")));
//    }
//
//    @Test
//    public void returnAllAcceptedReservationTest() throws Exception {
//        List<Reservation> reservationKey = Arrays.asList(new Reservation(1, restaurant, "ACCEPTED", "Email", LocalDateTime.now(), table));
//
//
//        doReturn(reservationKey).when(resService).getReservationsByStatus("ACCEPTED");
//
//        this.mockMvc.perform(get("/reservation?status=ACCEPTED"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$[*].status").value(hasItems("ACCEPTED")));
//    }
//
//    @Test
//    public void returnAllDeclinedReservationTest() throws Exception {
//        List<Reservation> reservationKey = Arrays.asList(new Reservation(1, restaurant, "DECLINED", "Email", LocalDateTime.now(), table));
//
//
//        doReturn(reservationKey).when(resService).getReservationsByStatus("DECLINED");
//
//        this.mockMvc.perform(get("/reservation?status=DECLINED"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andDo(MockMvcResultHandlers.print())
//                .andExpect(jsonPath("$[*].status").value(hasItems("DECLINED")));
//    }
//
//    @Test
//    public void returnAllNonNullStatusReservationTest() throws Exception {
//        List<Reservation> reservationKey = Arrays.asList(new Reservation(1, restaurant, "PENDING", "Email", LocalDateTime.now(), table));
//
//
//        doReturn(reservationKey).when(resService).getAllReservations();
//
//        this.mockMvc.perform(get("/reservation?status=NotNull"))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andDo(MockMvcResultHandlers.print())
//                .andExpect(jsonPath("$[*].status").value(hasItems("PENDING")));
//    }


    @Test
    public void createReservationTest() throws Exception {
        Reservation reservation = new Reservation(1, restaurant, "denied", "melanie.duah@revature.net", LocalDateTime.of(2021,7,31,22,10), table);
        System.out.println(reservation.toString());
        doReturn(reservation).when(resService).createNewReservation(reservation);

        this.mockMvc.perform(post("/reservation")
                .header("Content-Type", "application/json")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                //.content("{ \"id\":0, \"tableId\":1, \"status\":\"ACCEPTED\", \"email\":\"marc@test.test\", \"time\":\"2021-07-31T22:10\" }"))
                .content("{id\":1,\"restaurant\":{\"id\": 25,\"name\": \"La Hustle Inn\",\"address\":\"Hustle Ave\",\"cuisine\":\"Hustle\",\"rating\":null,\"photo\":\"\",\"placeId\": null,\"totalUserRatings\":0,\"priceLevel\":0},\"status\":\"denied\",\"email\":\"melanie.duah@revature.net\",\"reservationDatetime\":\"2021-07-31T22:10\",\"table\": {\"id\": 1,\"restaurant\": {\"id\": 25,\"name\":\"La Hustle Inn\",\"address\":\"Hustle Ave\",\"cuisine\":\"Hustle\",\"rating\":null,\"photo\":\"\",\"placeId\":null,\"totalUserRatings\":0,\"priceLevel\":0},\"capacity\": 10,\"status\": null}}"))
        .andDo(MockMvcResultHandlers.print());
        //.andExpect(status().isCreated());

    }

    @Test
    public void deleteReservationTest() throws Exception {

    }

    @Test
    public void updateReservationTest() throws Exception {

    }
}
