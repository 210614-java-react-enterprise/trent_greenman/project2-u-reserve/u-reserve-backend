package dev.team1.controllerTest;

import dev.team1.controllers.AdminController;
import dev.team1.controllers.ReservationController;
import dev.team1.controllers.RestaurantController;
import dev.team1.models.Restaurant;
import dev.team1.services.AdminService;
import dev.team1.services.ReservationService;
import dev.team1.services.RestaurantService;
import dev.team1.services.TableService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
public class RestaurantTest {

    private MockMvc mockMvc;

    @MockBean
    RestaurantService restaurantService;

    @Autowired
    RestaurantController restaurantController;

    @BeforeEach
    public void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(restaurantController).build();
    }
    @Test
    public void returnAllRestaurantsTest() throws Exception {
        List<Restaurant> restaurant = Arrays.asList(
                new Restaurant(1,"In-n-Out", "Arizona", "photo", "5", "placeId", 100, 5),
                new Restaurant(2,"Chick-fil-a", "Arizona", "photo", "5", "placeId", 100, 5),
                new Restaurant(3,"Arby's", "Arizona", "photo", "5", "placeId", 100, 5));

        doReturn(restaurant).when(restaurantService).getAllRestaurants();

        mockMvc.perform(get("/restaurant"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[*].name").value(hasItems("In-n-Out","Chick-fil-a","Arby's")));
    }

    @Test
    public void createRestaurantTest() throws Exception {
        //int id, String name, String location, String photo, String rating, String placeId, int totalUserRatings, int priceLevel
        Restaurant restaurant = new Restaurant(0, "Chipotle", "123 Fake Street", "Photo","5","PLACEID5", 100, 5);
        doReturn(restaurant).when(restaurantService).createNewRestaurant(restaurant);

        this.mockMvc.perform(post("/restaurant")
                .header("Authorization", "token")
                .characterEncoding("utf-8")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"name\":\"Marc\", \"location\":\"123 Fake Street\", \"rating\":\"5\", \"photo\":\"Photo\", \"placeId\":\"PlaceId\" }"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isCreated());
                //.andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void deleteRestaurantTest() throws Exception {
        Restaurant restaurant = new Restaurant(0, "Chipotle", "123 Fake Street", "Photo","5","PLACEID5", 100, 5);
        doReturn(restaurant).when(restaurantService).deleteRestaurant(restaurant);

        this.mockMvc.perform(delete("/restaurant")
                .header("Authorization", "token")
                .characterEncoding("utf-8")
                .contentType(MediaType.APPLICATION_JSON)
                //.content("{ \"name\":\"Marc\", \"location\":\"123 Fake Street\", \"rating\":\"5\", \"photo\":\"Photo\", \"placeId\":\"PlaceId\" }"))
                .content("{ \"id\":0}"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isNoContent());
    }

}
