package dev.team1.aspect;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class AuthenticationAspect {

    private Logger logger = LogManager.getLogger(LoggingAspect.class);

    //@Around("within(dev.team1.controllers.*)")
    @Around("execution(* dev.team1.controllers.ReservationController.returnAllReservations(..))"+
            " || execution(* dev.team1.controllers.ReservationController.updateReservation(..))")
    public ResponseEntity<?> authorizeRequest(ProceedingJoinPoint pjp) throws Throwable {
        HttpServletRequest request =
                ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();

        String token = request.getHeader("Authorization");
        if(token==null){
            logger.warn("no token received from request");
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        } else {
            logger.info("token received");
            return (ResponseEntity) pjp.proceed();
        }
    }

//    @Around("execution(dev.team1.comtrollers.ReservationController")
//    public ResponseEntity<?> authorizeAdminRequest(ProceedingJoinPoint pjp) throws Throwable {
//        HttpServletRequest request =
//                ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
//
//        String token = request.getHeader("Authorization");
//        if(token==null){
//            logger.warn("no token received from request");
//            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
//        } else if(token.equals("admin-token")){
//            logger.info("token received");
//            return (ResponseEntity) pjp.proceed();
//        } else {
//            logger.info("incorrect token for this action");
//            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
//        }
//    }

}
