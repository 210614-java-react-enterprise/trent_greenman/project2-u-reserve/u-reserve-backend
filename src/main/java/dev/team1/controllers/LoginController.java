package dev.team1.controllers;

import dev.team1.models.Administrator;
import dev.team1.services.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class LoginController {

    @Autowired
    private AdminService adminService;

    @PostMapping(value="/login", consumes = "application/x-www-form-urlencoded")
    public ResponseEntity<String> login(@RequestParam("username") String username,
                                        @RequestParam("password") String password){
        //check username and password against db records
        //List<Administrator> adminList = adminService.getAdminByUsernameAndPassword(username, password);
        //if credentials are present, return token
//        if(!adminList.isEmpty()){
//            return ResponseEntity.ok().header("Autorization", username+"-auth-token")
//                    .header("Access-Control-Expose-Headers", "Content-Type, Allow, Authorization").build();
//        }else{
//            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
//        }
        return null;
    }

    @GetMapping(value="/login", consumes = "application/x-www-form-urlencoded")
    public ResponseEntity<String> loginGet(@RequestParam("username") String username,
                                        @RequestParam("password") String password){
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
}
