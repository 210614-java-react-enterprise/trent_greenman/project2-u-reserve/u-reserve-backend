package dev.team1.controllers;

import dev.team1.models.Administrator;
import dev.team1.models.Reservation;
import dev.team1.services.AdminService;
import dev.team1.services.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/reservation")
public class ReservationController {
    @Autowired
    private ReservationService reservationService;

    @Autowired
    private AdminService adminService;

    //Sample Http Request: http://localhost:8081/reservation with Header: Authorization, token
    @GetMapping(produces = "application/json")

    public ResponseEntity<List<Reservation>> returnAllReservations(
            @RequestParam(value = "adminid",required = false)Integer id) {

        Administrator administrator = adminService.getAdminById(id);
        List<Reservation> reservations = null;
        HttpStatus status = HttpStatus.UNAUTHORIZED;
        if (administrator != null) {
            reservations = reservationService.getAllReservations();
            status = HttpStatus.OK;
        }
        return new ResponseEntity<>(reservations, status);
    }
    //Sample Http Request: http://localhost:8081/reservation with Header: Authorization, token
    //Body:
    //{
    //    "tableId": 1,
    //    "status": "APPROVED",
    //    "email": "trent@green.com",
    //    "time": "2012-04-23T18:25:43.511Z"
    //}
    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> createReservation(@RequestBody List<Reservation> reservations) {
        // reservation.setStatus("pending");
        for(Reservation reservation : reservations){
            reservation.setStatus("pending");
            reservationService.createNewReservation(reservation);
        }
        return new ResponseEntity<>("Reservations saved", HttpStatus.CREATED);
    }

    @DeleteMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<Reservation> deleteReservation(@RequestBody Reservation reservation) {
        System.out.println(reservation);
        return new ResponseEntity<>(reservationService.deleteReservation(reservation), HttpStatus.NO_CONTENT);
    }

    @PutMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<List<Reservation>> updateReservation(@RequestBody(required = false) List<Reservation> reservations) {
        List<Reservation> results = new ArrayList<>();
        for(Reservation reservation : reservations){
            results.add(reservationService.updateReservation(reservation));
        }
        return new ResponseEntity<>(results, HttpStatus.OK);

    }
}
