package dev.team1.controllers;

import dev.team1.models.Administrator;
import dev.team1.services.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private AdminService adminService;

    //Sample Http Request: http://localhost:8081/admin with Header: Authorization, token
    @PostMapping( consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> logInAsAdmin(@RequestBody Administrator admin){

        String loginResponse = "wrong user name or password";
        HttpStatus status = HttpStatus.UNAUTHORIZED;

        Administrator administrator = adminService.getAdminByUsernameAndPassword(admin.getUsername(),admin.getPassword());
        if(administrator != null){
            loginResponse = String.valueOf(administrator.getId());
            status = HttpStatus.OK;
        }
        return new ResponseEntity<>(loginResponse, status);
    }

    //Sample Http Request: http://localhost:8081/admin with Header: Authorization, token
    //Body:
//    {
//        "username": "melanie",
//        "password": "8910"
//    }

    @DeleteMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<Administrator> deleteAdmin(@RequestBody Administrator admin) {
        System.out.println(admin);
        return new ResponseEntity<>(adminService.deleteAdmin(admin), HttpStatus.NO_CONTENT);
    }
}
