package dev.team1.controllers;

import dev.team1.models.Reservation;
import dev.team1.models.Restaurant;
import dev.team1.models.TableReserve;
import dev.team1.services.ReservationService;
import dev.team1.services.TableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/table")
public class TableController {
    @Autowired
    private TableService tableService;

    @Autowired
    private ReservationService reservationService;

    //Sample Http Request: http://localhost:8081/table with Header: Authorization, token
    @GetMapping(produces = "application/json")
    public ResponseEntity<List<TableReserve>> returnAllTables(
            @RequestParam(value = "restaurantid",required = false)Integer restaurantId,
            @RequestParam(value = "datetime",required = false) String dateTime
    ) {
        List<TableReserve> restaurantTables = null;
        if(dateTime != null) {
            TemporalAccessor temporalAccessor = DateTimeFormatter.ISO_INSTANT.parse(dateTime);
            Instant instant = Instant.from(temporalAccessor);
            LocalDateTime reservationDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
            Restaurant restaurant = new Restaurant();
            restaurant.setId(restaurantId);
            restaurantTables = tableService.getAllTables(restaurant);
            List<TableReserve> alreadyReservedTables = tableService.getReservedTables(restaurant, reservationDateTime);
            for(TableReserve restaurantTable : restaurantTables){
                if(alreadyReservedTables.contains(restaurantTable))
                    restaurantTable.setStatus("Booked");
                else
                    restaurantTable.setStatus("Available");
            }
        }
        return new ResponseEntity<>(restaurantTables, HttpStatus.OK);
    }

    //Sample Http Request: http://localhost:8081/table with Header: Authorization, token
    //Body:
    //{
    //    "restaurantId": 2,
    //    "capacity": 4
    //}
    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<TableReserve> createTable(@RequestBody TableReserve table) {
        System.out.println(table);
        return new ResponseEntity<>(tableService.createNewTable(table), HttpStatus.CREATED);
    }

    @DeleteMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<TableReserve> deleteTable(@RequestBody TableReserve table) {
        System.out.println(table);
        return new ResponseEntity<>(tableService.deleteTable(table), HttpStatus.NO_CONTENT);
    }
}
