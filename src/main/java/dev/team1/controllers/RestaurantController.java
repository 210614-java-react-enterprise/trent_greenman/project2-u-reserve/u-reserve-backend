package dev.team1.controllers;
import dev.team1.models.Restaurant;
import dev.team1.services.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/restaurant")
public class RestaurantController {
    @Autowired
    private RestaurantService restaurantService;
    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    //Sample Http Request: http://localhost:8081/restaurant with Header: Authorization, token
    @GetMapping(produces = "application/json")
    public ResponseEntity<List<Restaurant>> returnAllRestaurants(@RequestParam(value = "name", required = false) String
                                                                         searchTerm) {
        return new ResponseEntity<>(restaurantService.getAllRestaurants(), HttpStatus.OK);
    }


    //Sample Http Request: http://localhost:8081/restaurant with Header: Authorization, token
    //Body:
    //{
    //    "name": "Fidels",
    //    "location": "San Diego"
    //}
    @PostMapping(consumes = "application/json", produces = "application/json")
    @ResponseBody
    public ResponseEntity<Restaurant> createRestaurant(@RequestBody Restaurant restaurant) {
        System.out.println(restaurant);
        return new ResponseEntity<>(restaurantService.createNewRestaurant(restaurant), HttpStatus.CREATED);
    }

    @DeleteMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<Restaurant> deleteRestaurant(@RequestBody Restaurant restaurant) {
        System.out.println(restaurant);
        return new ResponseEntity<>(restaurantService.deleteRestaurant(restaurant), HttpStatus.NO_CONTENT);
    }
}
