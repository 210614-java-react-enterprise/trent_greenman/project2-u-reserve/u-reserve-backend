package dev.team1.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.time.LocalDateTime;

@Component
@Entity
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer"})
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @JoinColumn(name="restaurant_id")
    @OneToOne
    private Restaurant restaurant;

    @Column(name = "status")
    private String status;

    @Column(name = "email")
    private String email;

    @Column(name = "reservationDatetime")
    private LocalDateTime reservationDatetime;

    @JoinColumn(name = "table_id")
    @OneToOne
    private TableReserve table;

    public Reservation() {
    }

    public Reservation(int id, Restaurant restaurant, String status, String email, LocalDateTime reservationDatetime, TableReserve table) {
        this.id = id;
        this.restaurant = restaurant;
        this.status = status;
        this.email = email;
        this.reservationDatetime = reservationDatetime;
        this.table = table;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDateTime getReservationDatetime() {
        return reservationDatetime;
    }

    public void setReservationDatetime(LocalDateTime reservationDatetime) {
        this.reservationDatetime = reservationDatetime;
    }

    public TableReserve getTable() {
        return table;
    }

    public void setTable(TableReserve table) {
        this.table = table;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "id=" + id +
                ", restaurant=" + restaurant +
                ", status='" + status + '\'' +
                ", email='" + email + '\'' +
                ", reservationDatetime=" + reservationDatetime +
                ", table=" + table +
                '}';
    }
}
