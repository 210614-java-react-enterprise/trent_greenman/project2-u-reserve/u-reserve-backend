package dev.team1.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.stereotype.Component;

import javax.persistence.*;

@Component
@Entity
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer"})
public class Restaurant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "restaurant_id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "location")
    private String address;

    @Column(name = "cuisine")
    private String cuisine;

    @Column(name = "rating")
    private String rating;

    @Column(name = "photo")
    private String photo;

    @Column(name = "place_id")
    private String placeId;

    @Column
    private int totalUserRatings;

    @Column
    private int priceLevel;

    public Restaurant(){
        super();
    }

    public Restaurant(int id, String name, String location, String photo, String rating, String placeId, int totalUserRatings, int priceLevel) {
        this.id = id;
        this.name = name;
        this.address = location;
        this.photo = photo;
        this.rating = rating;
        this.placeId = placeId;
        this.totalUserRatings = totalUserRatings;
        this.priceLevel = priceLevel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCuisine() {
        return cuisine;
    }

    public void setCuisine(String cuisine) {
        this.cuisine = cuisine;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public int getTotalUserRatings() {
        return totalUserRatings;
    }

    public void setTotalUserRatings(int totalUserRatings) {
        this.totalUserRatings = totalUserRatings;
    }

    public int getPriceLevel() {
        return priceLevel;
    }

    public void setPriceLevel(int priceLevel) {
        this.priceLevel = priceLevel;
    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", cuisine='" + cuisine + '\'' +
                ", rating='" + rating + '\'' +
                ", photo='" + photo + '\'' +
                ", placeId='" + placeId + '\'' +
                ", totalUserRatings=" + totalUserRatings +
                ", priceLevel=" + priceLevel +
                '}';
    }
}
