package dev.team1.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Objects;

@Component
@Entity
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer"})
public class TableReserve {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "table_id")
    private int id;

    @JoinColumn(name = "restaurant_id")
    @OneToOne
    private Restaurant restaurant;

    @Column(name = "capacity")
    private int capacity;

    private String status = "Available";

    public TableReserve() {
    }

    public TableReserve(int id, Restaurant restaurant, int capacity, String status) {
        this.id = id;
        this.restaurant = restaurant;
        this.capacity = capacity;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TableReserve)) return false;
        TableReserve that = (TableReserve) o;
        return id == that.id && capacity == that.capacity && restaurant.equals(that.restaurant);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, restaurant, capacity);
    }

    @Override
    public String toString() {
        return "TableReserve{" +
                "id=" + id +
                ", restaurant=" + restaurant +
                ", capacity=" + capacity +
                '}';
    }

}
