package dev.team1.daos;

import dev.team1.models.Administrator;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AdminRepository extends JpaRepository<Administrator,Integer> {

   Administrator findByUsernameAndPassword(String username, String password);
}
