package dev.team1.daos;

import dev.team1.models.Restaurant;
import dev.team1.models.TableReserve;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface TableRepository extends JpaRepository<TableReserve,Integer> {
    List<TableReserve> findByRestaurant(Restaurant restaurant);

    @Query(value="SELECT tr.table_id,tr.restaurant_id,tr.capacity FROM table_reserve AS tr JOIN reservation AS r ON r.table_id=tr.table_id WHERE tr.restaurant_id = :restaurantid AND r.reservation_datetime > :reservationdatetime", nativeQuery = true)
    List<TableReserve> findByReservation(@Param("restaurantid") Integer restaurant,
                                         @Param("reservationdatetime")LocalDateTime reservationDate);
}
