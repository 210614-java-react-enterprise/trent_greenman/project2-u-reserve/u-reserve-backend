package dev.team1.daos;

import dev.team1.models.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReservationRepository extends JpaRepository<Reservation,Integer> {

    Reservation findById(int id);
    List<Reservation> findByStatus(String status);

}
