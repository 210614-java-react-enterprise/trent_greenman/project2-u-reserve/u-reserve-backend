package dev.team1.services;

import dev.team1.daos.TableRepository;
import dev.team1.models.Restaurant;
import dev.team1.models.TableReserve;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class TableService {
    @Autowired
    private TableRepository tableRepository;

    public List<TableReserve> getAllTables(Restaurant restaurant){
        return tableRepository.findByRestaurant(restaurant);
    }

    public TableReserve createNewTable(TableReserve t){
        return tableRepository.save(t);
    }

    public TableReserve deleteTable(TableReserve t) {
        tableRepository.delete(t);
        return t;
    }

    public List<TableReserve> getReservedTables(Restaurant restaurant, LocalDateTime reservationDateTime) {
        return tableRepository.findByReservation(restaurant.getId(),reservationDateTime);
    }
}
