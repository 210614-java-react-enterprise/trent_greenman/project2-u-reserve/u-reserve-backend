package dev.team1.services;

import dev.team1.daos.AdminRepository;
import dev.team1.models.Administrator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminService {

    @Autowired
    private AdminRepository adminRepository;

    public List<Administrator> getAllAdmin(){
        return adminRepository.findAll();
    }

    public Administrator getAdminByUsernameAndPassword(String username, String password){
        return adminRepository.findByUsernameAndPassword(username, password);
    }

    public Administrator getAdminById(Integer id){
        return adminRepository.findById(id).orElse(null);
    }

    public Administrator createNewAdmin(Administrator a){
        return adminRepository.save(a);
    }

    public Administrator deleteAdmin(Administrator a){
        adminRepository.delete(a);
        return a;
    }
}
