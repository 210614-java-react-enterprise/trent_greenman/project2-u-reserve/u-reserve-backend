package dev.team1.services;

import dev.team1.daos.ReservationRepository;
import dev.team1.daos.RestaurantRepository;
import dev.team1.models.Reservation;
import dev.team1.models.Restaurant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReservationService {
    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private RestaurantRepository restaurantRepository;

    public List<Reservation> getAllReservations(){
        return reservationRepository.findAll();
    }

    public Reservation getReservationById(int id){
        Reservation reservation = reservationRepository.findById(id);
        return reservation;
    }

//    public List<Reservation> getReservationsByTableId(int tableId){
//        return reservationRepository.findByTableId(tableId);
//    }

    public List<Reservation> getReservationsByStatus(String status){
        return reservationRepository.findByStatus(status);
    }

    public Reservation createNewReservation(Reservation reservation){
        Restaurant restaurant = restaurantRepository.getOne(reservation.getId());
        restaurantRepository.save(restaurant);
        return reservationRepository.save(reservation);
    }

    public Reservation deleteReservation(Reservation r) {
        reservationRepository.delete(r);
        return r;
    }

    public Reservation updateReservation(Reservation r) {
        return reservationRepository.save(r);
    }

}
