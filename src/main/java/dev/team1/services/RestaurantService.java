package dev.team1.services;

import dev.team1.daos.RestaurantRepository;
import dev.team1.models.Restaurant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RestaurantService {
    @Autowired
    private RestaurantRepository restaurantRepository;

    public List<Restaurant> getAllRestaurants(){
        return restaurantRepository.findAll();
    }

    public Restaurant createNewRestaurant(Restaurant r){
        return restaurantRepository.save(r);
    }

    public Restaurant deleteRestaurant(Restaurant r) {
        restaurantRepository.delete(r);
        return r;
    }
}
