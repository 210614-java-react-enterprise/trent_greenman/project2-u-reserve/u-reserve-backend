# U-Reserve Backend

This backend for the U-Reserve project implements the Spring Boot framework.\
The documentation for Spring Boot can be found here: [https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/)

## Controllers, Models, Services, and DAOs

Each of our models include `Administrator`, `Reservation`, `TableReserve`, and `Restaurant` each `Reservation` is linked to a `TableReserve` and each `TableReserve` is linked to a `Restaurant`. We do this with Table ID's and Restaurant ID's. Throughout each of the controllers, we have the ability to perform necessary CRUD operations.

## Testing
For the testing of our application, we wrote tests for the services and controllers. For the service layer, we were able to reach 80% line coverage as demonstrated by our JaCoCo reports. Within each test, we used mocks to deal with objects that had dependencies.\
The documention for JaCoCo can be found here: [https://www.jacoco.org/jacoco/trunk/doc/](https://www.jacoco.org/jacoco/trunk/doc/)
